import sys,time

#gpio.setmode(gpio.BOARD)
print sys.argv
pin = map(int, sys.argv[-3].split(",") )
uptime = float(sys.argv[-2])
count = int(sys.argv[-1])

import pdelib
import threading

pdelib.setBoardAndPins(pin)
ts = []
for p in pin:
	t =threading.Thread(
		target= pdelib.intermit,
		args = (p,uptime*2,count)
	)
	t.start()
	ts.append(t)
	time.sleep(uptime/1.8)

for t in ts:
	t.join()


