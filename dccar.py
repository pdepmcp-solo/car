#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
MotorPin1 = 11 # pin11
MotorPin2 = 12 # pin12
MotorEnable = 13 # pin13

WHEELLEFT = 'left'
WHEELRIGHT = 'right'

motorpins={
	WHEELLEFT:{'enable':13,
		'fw': 11,
		'bw': 12},
        WHEELRIGHT:{'enable':40,
                'fw': 37,
                'bw': 38}
}

def setup():
	GPIO.setmode(GPIO.BOARD) # Numbers GPIOs by physical location
	for w in [WHEELLEFT,WHEELRIGHT]:
		pins = motorpins[w]
		GPIO.setup(pins['enable'],GPIO.OUT)
                GPIO.setup(pins['fw'],GPIO.OUT)
                GPIO.setup(pins['bw'],GPIO.OUT)
		GPIO.output(pins['enable'],GPIO.LOW)

#	GPIO.setup(MotorPin1, GPIO.OUT) # mode --- output
#	GPIO.setup(MotorPin2, GPIO.OUT)
#	GPIO.setup(MotorEnable, GPIO.OUT)
#	GPIO.output(MotorEnable, GPIO.LOW) # motor stop

def goFW(wheel,dt):
	print wheel	
	for w in wheel:
		pins = motorpins[w]
        	GPIO.output(pins['enable'], GPIO.HIGH) # motor driver enable
	        GPIO.output(pins['fw'], GPIO.HIGH) # clockwise
        	GPIO.output(pins['bw'], GPIO.LOW)
        
	time.sleep(dt)
	for w in wheel:
		pins = motorpins[w]
	        GPIO.output(pins['enable'], GPIO.LOW) # motor stop


def goBW(wheel,dt):
        print wheel
        for w in wheel:
                pins = motorpins[w]
                GPIO.output(pins['enable'], GPIO.HIGH) # motor driver enable
                GPIO.output(pins['fw'], GPIO.LOW) # clockwise
                GPIO.output(pins['bw'], GPIO.HIGH)

        time.sleep(dt)
        for w in wheel:
                pins = motorpins[w]
                GPIO.output(pins['enable'], GPIO.LOW) # motor stop
	

def loop():
	while True:
		print 'Press Ctrl+C to end the program...'
		GPIO.output(MotorEnable, GPIO.HIGH) # motor driver enable
		GPIO.output(MotorPin1, GPIO.HIGH) # clockwise
		GPIO.output(MotorPin2, GPIO.LOW)
		time.sleep(5)
	
		GPIO.output(MotorEnable, GPIO.LOW) # motor stop
		time.sleep(5)

		GPIO.output(MotorEnable, GPIO.HIGH) # motor driver enable
		GPIO.output(MotorPin1, GPIO.LOW) # anticlockwise
		GPIO.output(MotorPin2, GPIO.HIGH)
		time.sleep(5)

		GPIO.output(MotorEnable, GPIO.LOW) # motor stop
		time.sleep(5)

def destroy():
	GPIO.output(MotorEnable, GPIO.LOW) # motor stop
	GPIO.cleanup() # Release resource

if __name__ == '__main__': # Program start from here
	#GPIO.cleanup()
	setup()
	try:
		goFW([WHEELLEFT],1)
		goFW([WHEELRIGHT],2)
		goBW([WHEELLEFT,WHEELRIGHT],2)
		goFW([WHEELLEFT,WHEELRIGHT],1)
		goBW([WHEELRIGHT],1)
		#loop()

	except KeyboardInterrupt: # When 'Ctrl+C' is pressed, the child program destroy() will be executed.
		destroy()
